package com.post_ms.controllers;
import com.post_ms.models.post;
import com.post_ms.repositories.postRepository;
import org.springframework.web.bind.annotation.*;
@RestController
public class postController {
    private final postRepository postRepository;
    public postController(postRepository postRepository) {
        this.postRepository = postRepository;
    }
    @PostMapping("/post")
    post newPost(@RequestBody post Post){
        return postRepository.save(Post);
    }
}