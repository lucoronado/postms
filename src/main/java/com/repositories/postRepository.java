package com.post_ms.repositories;

import com.post_ms.models.post;
import org.springframework.data.mongodb.repository.MongoRepository;
public interface postRepository extends MongoRepository <post, String> { }