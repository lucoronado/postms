package com.post_ms.repositories;

import com.post_ms.models.interest;
import org.springframework.data.mongodb.repository.MongoRepository;
public interface interestRepository extends MongoRepository <interest, String> { }