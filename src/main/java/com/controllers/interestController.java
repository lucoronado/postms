package com.post_ms.controllers;
import com.post_ms.exceptions.interestException;
import com.post_ms.models.interest;
import com.post_ms.repositories.interestRepository;
import org.springframework.web.bind.annotation.*;
@RestController
public class interestController {
    private final interestRepository interestRepository;
    public interestController(interestRepository interestRepository) {
        this.interestRepository = interestRepository;
    }
    @GetMapping("/interest/{userName}")
    interest getInterest(@PathVariable String userName){
        return interestRepository.findById(userName).orElseThrow(() -> new interestException("No se encontro una cuenta con el username: " + userName));
    }
    @PostMapping("/interest")
    interest newInterest(@RequestBody interest Interest){
        return interestRepository.save(Interest);
    }
}